---
title: "Refugis Climàtics dels Municipis de Catalunya"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: fill
    social: menu
    source_code: embed
    self_contained: FALSE
    theme: flatly
---


```{r setup, include=FALSE}
# Html widgets
library(tidyverse)
library(tibble)
library(flexdashboard)
library(leaflet)
#library(plotly)
library(crosstalk)
if (!require("summarywidget")) {renv::install("kent37/summarywidget")}; library(summarywidget)
library(htmltools)
library(DT)
library(janitor)
# Spatial data
library(sf)

# Tidyverse and such
library(dplyr)
library(readxl)
library(tidyr)
library(stringr)

# Visualisation and extras
library(viridis)
library(leaflet.extras)
library(leaflet.extras2)
library(leafem)
library(leaflet.providers)

# Taken from
#https://gist.github.com/timelyportfolio/866f2507ee2a8c50809cfacba575416b
valueBoxSummaryWidget <- function (value, caption = NULL, icon = NULL, color = NULL, href = NULL) 
{
    if (!is.null(color) && color %in% c("primary", "info", 
        "success", "warning", "danger")) 
        color <- paste0("bg-", color)
    valueOutput <- tags$span(class = "value-summarywidget-output", `data-caption` = caption, 
        `data-icon` = icon, `data-color` = color, 
        `data-href` = href, value)
    hasPrefix <- function(x, prefix) {
        if (!is.null(x)) 
            grepl(paste0("^", prefix), x)
        else FALSE
    }
    fontAwesome <- hasPrefix(icon, "fa")
    ionicons <- hasPrefix(icon, "ion")
    deps <- flexdashboard:::html_dependencies_fonts(fontAwesome, ionicons)
    if (length(deps) > 0) 
        valueOutput <- attachDependencies(valueOutput, deps)
    valueOutput
}

knitr::opts_chunk$set(cache = TRUE)
```

<script>
window.FlexDashboardComponents.push({

  type: 'custom',

  find: function(container) {
    if (container.find('span.value-summarywidget-output').length)
      return container;
    else
      return $();
  },

  flex: function(fillPage) {
    return false;
  },

  layout: function(title, container, element, fillPage) {

    // alias variables
    var chartTitle = title;
    var valueBox = element;

    // add value-box class to container
    container.addClass('value-box');

    // value paragraph
    var value = $('<p class="value"></p>');

    // if we have shiny-text-output then just move it in
    var valueOutputSpan = [];
    var shinyOutput = valueBox.find('.shiny-valuebox-output').detach();
    var summaryOutput = valueBox.find('.summarywidget').detach();
    
    if (shinyOutput.length) {
      valueBox.children().remove();
      shinyOutput.html('&mdash;');
      value.append(shinyOutput);
    }
    
    if (summaryOutput.length) {
      value.append(summaryOutput);
      valueOutputSpan = valueBox.find('span.value-summarywidget-output')
    }

    // caption
    var caption = $('<p class="caption"></p>');
    caption.append(chartTitle);

    // build inner div for value box and add it
    var inner = $('<div class="inner"></div>');
    inner.append(value);
    inner.append(caption);
    valueBox.append(inner);

    // add icon if specified
    var icon = $('<div class="icon"><i></i></div>');
    valueBox.append(icon);
    function setIcon(chartIcon) {
      var iconLib = '';
      var iconSplit = chartIcon.split(' ');
      if (iconSplit.length > 1) {
        iconLib = iconSplit[0];
        chartIcon = iconSplit.slice(1).join(' ');
      } else {
        var components = chartIcon.split('-');
        if (components.length > 1)
          iconLib = components[0];
      }
      icon.children('i').attr('class', iconLib + ' ' + chartIcon);
    }
    var chartIcon = valueBox.attr('data-icon');
    if (chartIcon)
      setIcon(chartIcon);

    // set color based on data-background if necessary
    var dataBackground = valueBox.attr('data-background');
    if (dataBackground)
      valueBox.css('background-color', bgColor);
    else {
      // default to bg-primary if no other background is specified
      if (!valueBox.hasClass('bg-primary') &&
          !valueBox.hasClass('bg-info') &&
          !valueBox.hasClass('bg-warning') &&
          !valueBox.hasClass('bg-success') &&
          !valueBox.hasClass('bg-danger')) {
        valueBox.addClass('bg-primary');
      }
    }

    // handle data attributes in valueOutputSpan
    function handleValueOutput(valueOutput) {

      // caption
      var dataCaption = valueOutput.attr('data-caption');
      if (dataCaption)
        caption.html(dataCaption);

      // icon
      var dataIcon = valueOutput.attr('data-icon');
      if (dataIcon)
        setIcon(dataIcon);

      // color
      var dataColor = valueOutput.attr('data-color');
      if (dataColor) {
        if (dataColor.indexOf('bg-') === 0) {
          valueBox.css('background-color', '');
          if (!valueBox.hasClass(dataColor)) {
             valueBox.removeClass('bg-primary bg-info bg-warning bg-danger bg-success');
             valueBox.addClass(dataColor);
          }
        } else {
          valueBox.removeClass('bg-primary bg-info bg-warning bg-danger bg-success');
          valueBox.css('background-color', dataColor);
        }
      }

      // url
      var dataHref = valueOutput.attr('data-href');
      if (dataHref) {
        valueBox.addClass('linked-value');
        valueBox.off('click.value-box');
        valueBox.on('click.value-box', function(e) {
          window.FlexDashboardUtils.showLinkedValue(dataHref);
        });
      }
    }

    // check for a valueOutputSpan
    if (valueOutputSpan.length > 0) {
      handleValueOutput(valueOutputSpan);
    }

    // if we have a shinyOutput then bind a listener to handle
    // new valueOutputSpan values
    shinyOutput.on('shiny:value',
      function(event) {
        var element = $(event.target);
        setTimeout(function() {
          var valueOutputSpan = element.find('span.value-output');
          if (valueOutputSpan.length > 0)
            handleValueOutput(valueOutputSpan);
        }, 10);
      }
    );
  }
});
</script>


Visualitzador {data-orientation=rows data-icon="fa-map"}
===================================== 

```{r echo=FALSE}
# Llegim l'Excel base
# RC = Refugis climàtics

ruta_base_arxiu_rc <-  "."
ruta_rel_arxiu_rc <- "Refugis_clim_tics_dels_municipis_de_Catalunya_20240729.csv"
arxiu_rc <- file.path(ruta_base_arxiu_rc, ruta_rel_arxiu_rc)

# Llegim les dades, netegem els noms, traiem els que no són refugi climàtic (que tampoc tenien coordenades anyways), i afegim un identificador de cada registre (no en tenien?): rc_id (identificador de refugi climàtic)
rc <- read_csv(arxiu_rc) %>% 
  clean_names() %>% 
  filter(refugi_climatic=="Sí") %>% 
  rownames_to_column(var="rc_id")
  
 
#class(rc)
# Convertim el dataframe en objecte sf en identificar la columna que conté les geoposicions via Well Known Text (WKT), tot afegint que les coordenades són en WGS84 (lon lat) epsg 4326
rc_sf <- st_as_sf(rc, wkt = "georeferencia", crs=4326)

#class(rc_sf)

# Definim la llegenda de colors
rc_llegenda <- tribble(
  ~tipus, ~color,
  "Altres",  "lightgreen",
  "Biblioteca",  "brown",
  "Centre educatiu",  "purple",
  "Equipament cívic",  "cyan",
  "Espai cultural",  "red", 
  "Espai firal",  "pink",
  "Ludoteca",  "black",
  "Parc densament arbrat",  "darkgreen",
  "Piscina",  "blue",
  "Poliesportiu",  "orange",
  "Sala polivalent",  "yellow"
)

rc_sf <- rc_sf %>% 
  filter(!is.na(latitud) & !is.na(longitud)) %>% 
  mutate(
    rc_color = case_when(
      tipologia == rc_llegenda$tipus[1] ~ rc_llegenda$color[1],
      tipologia == rc_llegenda$tipus[2] ~ rc_llegenda$color[2],
      tipologia == rc_llegenda$tipus[3] ~ rc_llegenda$color[3],
      tipologia == rc_llegenda$tipus[4] ~ rc_llegenda$color[4],
      tipologia == rc_llegenda$tipus[5] ~ rc_llegenda$color[5],
      tipologia == rc_llegenda$tipus[6] ~ rc_llegenda$color[6],
      tipologia == rc_llegenda$tipus[7] ~ rc_llegenda$color[7],
      tipologia == rc_llegenda$tipus[8] ~ rc_llegenda$color[8],
      tipologia == rc_llegenda$tipus[9] ~ rc_llegenda$color[9],
      tipologia == rc_llegenda$tipus[10] ~ rc_llegenda$color[10],
      tipologia == rc_llegenda$tipus[11] ~ rc_llegenda$color[11],
      TRUE ~ "white") 
  )

#tabyl(rc, tipologia) 

# Ens quedem amb una còpia del dataset de refugis en format planar (etrs89, UTM fus 31; epsg 25831) per a quan vulguem fer analisi de cobertures al territori (isocrones a N minuts caminant, o M minuts en vehicle a motor)
rc_sf.etrs89 <-  rc_sf %>%  st_transform(25831) 

# Definim el Shared Data Crosstalk object (sdc)
sdc <- SharedData$new(rc_sf)

```

Row {data-height=550}
------------------------------------



### Filtres {data-width=250}

```{r}

bscols(
  widths = c(1,4,6,1),
    list(
      
    ),
    list(
      filter_select("comarca", "Comarca", sdc, group=~comarca),
      filter_select("municipi", "Municipi", sdc, group=~municipi),
      filter_checkbox("tipologia", "Tipus d'Equipament", sdc, group=~tipologia, inline=TRUE)
    ),
    list(
      filter_checkbox("gratuitat", "Gratuïtat", sdc, group=~gratuitat, inline=TRUE),
      filter_checkbox("accessibilitat", "Accessibilitat", sdc, group=~accessibilitat, inline=TRUE),
      filter_checkbox("aigua", "Té aigua de beure?", sdc, group=~aigua, inline=TRUE),
      filter_checkbox("lavabo", "Té lavabo?", sdc, group=~lavabo, inline=TRUE),
      filter_checkbox("mobiliari_per_seure", "Té mobiliari per seure?", sdc, group=~mobiliari_per_seure, inline=TRUE),
      filter_slider("capacitat", "Capacitat", sdc, column=~capacitat, step=1)
    ),
      list(
      
    )
)
```

### Total {data-width=50}

```{r}
valueBox(
  nrow(rc_sf),
  icon='fa-building'
)
```

### Selecció {data-width=50}

```{r}
valueBoxSummaryWidget(
  icon= 'fa-building',
  summarywidget(
    sdc,
    statistic = 'count',
    column = 'rc_id',
    digits = 0
  ),
  color = "purple"
)
```

### Mapa interactiu


```{r}
bm <- leaflet(sdc, width="100%", height="100%") %>%
  
  # OS map layer
  addProviderTiles(providers$Esri.WorldImagery, group="ESRI Satellite",
                 options=leafletOptions(maxNativeZoom=19,maxZoom=100)) %>%
  addProviderTiles("OpenStreetMap",
           options=leafletOptions(maxNativeZoom=19,maxZoom=100)) %>%
  addProviderTiles("CartoDB.Positron",
           options=leafletOptions(maxNativeZoom=19,maxZoom=100)) %>%

  # Sample points
  addCircleMarkers(data=sdc, radius=4, weight=1, color=~rc_color, 
                   label = ~htmlEscape(nom), popup = ~horari, group="refugis") %>%
 
#   addPolygons(data=sf_parcs_ok, color = "green", weight = 1, smoothFactor = 0.5,
#     opacity = 1.0, fillOpacity = 0.5) %>% 
# 
  # Add layer control elements
  addLayersControl(baseGroups = c("CartoDB.Positron", "OpenStreetMap", "ESRI Satellite"),
                   options = layersControlOptions(collapsed = TRUE,
                                                  autoZIndex = F)) %>% 
  addLegend(
    position = "bottomright",
    colors = rc_llegenda$color,
    labels = rc_llegenda$tipus,
    layerId = "basegroup_legend",
    title='Tipologia de refugis climàtics') %>% 
  
  addEasyprint(options = easyprintOptions(
        title = 'Give me that map',
        position = 'bottomleft',
        exportOnly = TRUE,
        # hideClasses = list("leaflet-overlay-pane", "leaflet-popup"),
        # hidden = FALSE, hideControlContainer = TRUE,
        filename = "mapit",
        tileLayer = "basemap",
        tileWait = 5000,
        defaultSizeTitles = list(
          "CurrentSize" = "The current map extent",
          "A4Landscape" = "A4 (Landscape) extent with w:1045, h:715",
          "A4Portrait" = "A4  (Portrait) extent with w:715, h:1045"
        ),
        # sizeModes = c("A4Portrait","A4Landscape"),
        sizeModes = list("CurrentSize" = "CurrentSize",
                         "A4Landscape" = "A4Landscape",
                         "A4Portrait" = "A4Portrait",
                         "Custom Landscape"=list(
                           width= 1800,
                           height= 700,
                           name = "A custom landscape size tooltip",
                           className= 'customCssClass'),
                         "Custom Portrait"=list(
                           width= 700,
                           height= 1800,
                           name = "A custom portrait size tooltip",
                           className= 'customCssClass1')
        ),
        customWindowTitle = "Some Fancy Title",
        customSpinnerClass = "shiny-spinner-placeholder",
        spinnerBgColor = "#b48484")) %>%
  
  # addControl("<P><B>Pista!</B> Cerca ...<br/><ul><li>CCCB</li><li>CUESB</li><li>Piscina</li>
  #            <li>Biblioteca</li><li>Centre cívic</li><li>Casal de Barri</li></ul></P>",
  #            position = "bottomright") %>% 
  
  addSearchFeatures(
    targetGroups = "refugis",
    options = searchFeaturesOptions(
      zoom = 12, openPopup = TRUE, firstTipSubmit = TRUE,
      autoCollapse = TRUE, hideMarkerOnCollapse = TRUE )) %>% 
  
  addControlGPS(
    options = gpsOptions(
      autoCenter=TRUE,
      setView=TRUE
      )
    ) %>% 
  
  addFullscreenControl(
    position = "topleft", 
    pseudoFullscreen = FALSE
    )

bm
```


Row {data-height=400}
------------------------------------

### Llistat

```{r width='100%'}
DT::datatable(sdc, 
              extensions = c(
                "Buttons",  # add download buttons, etc
                "Scroller"  # for scrolling down the rows rather than pagination
              ),
              height='100%', width='100%', 
              rownames = FALSE,
              style="bootstrap", class="compact",
              filter= 'top',
              options = list(
                dom = 'Brtip',
                buttons = c('csv', 'excel'),
                deferRender=TRUE, 
                scrolly=80,
                scroller=TRUE,
                language = list(url = 'https://cdn.datatables.net/plug-ins/1.10.11/i18n/Catalan.json'),
                columnDefs = list(
                  list(
                    visible = FALSE, targets = c(0, 3:6, 19:20) # Aquí estic 
                  )
                ),
                initComplete = JS(
                  "function(settings, json) {",
                  "$(this.api().table().header()).css({'background-color': '#000', 'color': '#fff'});",
                  "}")
                
              )
)

```


Informació {.storyboard data-icon="fa-info-circle"}
==================================== 

### Dades

Dades tretes del portal de Transparència de la Generalitat de Catalunya: "Refugis Climàtics dels Municipis de Catalunya"
https://analisi.transparenciacatalunya.cat/Seguretat/Refugis-clim-tics-dels-municipis-de-Catalunya/9gu7-iwci/about_data

### Programa

El programa s'ha desenvolupat íntegrament amb eines de codi obert (programari lliure), per qüestions filosòfiques i de sobirania tecnològica tant de la ciutadania com de l'administració pública. 

Fonamentalment emprant el llenguatge de programació **R** ( https://r-project.org )

Es pot obtenir el codi font del programa a:

https://gitlab.com/radup/climate-shelters/

### Crèdits

Programa desenvolupat per Xavier de Pedro Puente, en hores lliures com a ciutadà i de forma voluntària (fora de la seva feina com a funcionari públic).

### Copyright 

Programa compartit amb llicència MIT.

### Contacte

xavidp @ gitlab, linkedin . O via aquest formulari:

https://seeds4c.org/contact

### Agraïments

Els autors de l'exemple en que em vaig guiar per fer aquest visualitzador:

https://ecodiv.earth/demos/demo_schijndelvoedselbos.html

Kent Rushell a Timelyportfolio, pel codi customitzat per mostrar els recomptes filtrats tot via javascript:

https://gist.github.com/timelyportfolio/866f2507ee2a8c50809cfacba575416b

Els autors dels paquets de R emprats per generar aquest visualitzador (molts!)

Les persones que van compilar les dades dels refugis climàtics dels municipis de Catalunya, i les van posar al portal de Transparència de la Generalitat.

Les persones que hi ha darrera de tots i cadascun dels refugis climàtics mostrats. 

Les persones visionàries de l'Ajuntament de Barcelona que al 2019 van veure que aquest tipus d'espais eren necessaris i van començar a fer crèixer la llavor a la ciutat de Barcelona. I a totes les que anys posteriors han anat millorant la xarxa d'espais de refugi climàtic a la ciutat de Barcelona. Més informació a:

https://barcelona.cat/refugisclimatics
