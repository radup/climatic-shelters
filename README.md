# Climate Shelters

Proof-of-concept apps to provide ways to display climate shelter data using free/open source software, with more technological sovereignty.


## Climate Shelters in Barcelona 

An alternative way to expose/display the climate shelters in Barcelona (Catalonia, Spain) 

As a reference, this is the official website from the Barcelona City Council: 

https://barcelona.cat/refugisclimatics

Dataset can be taken on the fly from the corresponding opendata portal

https://opendata-ajuntament.barcelona.cat/data/ca/dataset/xarxa-refugis-climatics

All using the open source software R and its packages

https://www.r-project.org/


### Using Shiny server (R)

Produce some web app out of it, in order to map/view/filter/list them and check opening hours, distribution by city districts and neighbourhoods, etc.  

![Climatic Shelter Network in Barcelona, as of June 2024](images/bcn_app_scheenshot_shiny_01.png)

See:

https://gitlab.com/radup/climate-shelters/-/tree/main/app_Climate_Shelters_BCN_Shiny/

### Using just javascript

Work in progreess here:

https://gitlab.com/radup/climate-shelters/-/tree/main/app_Climate_Shelters_BCN_crosstalk_plotly_dt/

However, it's temporarily discontinued, while a the proof of concept is made with improvements using the broader dataset for the whole Catalonia (see below)

## Climate Shelters in all Catalonia 

Using just html and javascript, created from R packages.

Work in progreess here:

https://gitlab.com/radup/climate-shelters/-/tree/main/app_Climate_Shelters_CAT/

![Climatic Shelter Network in Catalonia, as of July 29 2024](images/cat_app_flexdashboard_crosstalk_03_leaflet_goodies.png)

You can select shelters visually through the map, using the provided square-selection tool:

![Filter by means of square-drawing tool](images/cat_app_flexdashboard_crosstalk_02_seleccio_x_requadre_en_mapa.png)

Dataset: opendata file from the Catalan Government transparency site.

https://analisi.transparenciacatalunya.cat/Seguretat/Refugis-clim-tics-dels-municipis-de-Catalunya/9gu7-iwci/about_data

### Demo

You can see the app in action here:

https://datascience.seeds4c.org/refugisclimatics/

(not in English, sorry, but I hope it's enough as a proof-of-concept ;-) 

## Comments?

Feel free to use/adapt/improve (FLOSS & "Public money, public code", etc). 

And report/comment if you like it or even if you plan to extend it for your use case.

xavidp @ gitlab, linkedin 
